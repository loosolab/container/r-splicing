FROM rocker/tidyverse:latest
LABEL maintainer="mario.looso@mpi-bn.mpg.de"

#Hack to read custom setupR
COPY . /app

RUN \
    # Install additional tools
    ## Assure up-to-date package lists
    apt-get update && \
    apt-get dist-upgrade --assume-yes && \
    apt-get install --assume-yes --no-install-recommends \
    ## Fetch additional libraries
      libssh2-1-dev libgit2-dev libglpk40 \
      ## Fetch the additional tools
      git-lfs gnupg ssh-client \
      # Install libraries needed for compilation of the autonomics toolkit
      ## Dependencies of rgl
      libx11-dev libglu1-mesa-dev \
      ## Dependencies of roxygen2
      libxml2-dev zlib1g-dev \
      ## Dependencies of multipanelfigure (etc.)
      libmagick++-dev && \
    # Clean the cache(s)
    apt-get clean && \
    rm -r /var/lib/apt/lists/*

#RUN \
 #   sudo -u rstudio Rscript -e 'source("https://gitlab.gwdg.de/loosolab/container/r-data.analysis/raw/master/tinytex.R")'
RUN \
    sudo -u rstudio Rscript -e 'source("https://gitlab.gwdg.de/loosolab/container/r-splicing/-/raw/main/packages_bioc.R")'
RUN \
    # Set the time zone
    sh -c "echo 'Europe/Berlin' > /etc/timezone" && \
    dpkg-reconfigure -f noninteractive tzdata && \
    # Clean /tmp (downloaded packages)
    rm -r /tmp/* && \
    # Generate an ssh key
    sudo -u rstudio ssh-keygen -t ed25519 -N "" -f /home/rstudio/.ssh/id_ed25519
    
ENV \
    ROOT=TRUE \
    DISABLE_AUTH=TRUE \

