# r-splicing

R container with packages according to K.Z for splicing analysis, used in the BCU software self service section

## Getting started


```
git clone  https://gitlab.gwdg.de/loosolab/container/r-splicing.git
cd r-splicing
docker build -t docker.gitlab.gwdg.de/loosolab/container/r-splicing .
docker push docker.gitlab.gwdg.de/loosolab/container/r-splicing
```

### Running the container

```
docker run -d -p YOURIP:8787:8787 -e USERID=$UID -v /path/to/workspace:/home/rstudio/ docker.gitlab.gwdg.de/loosolab/container/r-splicing:latest
```

### Available packages
All packages from the [rocker/tidyverse](https://hub.docker.com/r/rocker/tidyverse) plus

"bioc_version = “3.15”
BioC package

bioc_packages <- c(“ComplexHeatmap”,
“biomaRt”,
“multipanelfigure”,
“BiocStyle”,
“plotly”,
“DESeq2”,
“pcaExplorer”,
“BSgenome”,
“GenomicRanges”,
“GenomicFeatures”,
“BSgenome.Mmusculus.UCSC.mm10",
“BSgenome.Mmusculus.UCSC.mm39”,
“rtracklayer”,
“clusterProfiler”,
“Gviz”,
“seqLogo”)
CRAN packages

cran_packages <- c(“pheatmap”,
“RColorBrewer”,
“ggplot2",
“magrittr”,
“reshape2",
“dplyr”,
“ggrastr”,
“ggrepel”,
“ggpointdensity”,
“ggVennDiagram”,
“viridis”,
“grDevices”"



